﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Formulario.aspx.cs" Inherits="CheckBoxList.Formulario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Formulário</title>
</head>
<style>
    body {
        background-color: #e4e4e4;
    }

    h1, h2 {
        text-align: center;
    }

    .formulario {
        display: flex;
        justify-content: center;
    }

    .btn {
        display: flex;
        justify-content: center;
        margin-top: 5px;
    }

    .lbl {
        justify-content: center;
        display: flex;
        margin-top: 10px;
        font-size: 20px;
        font-weight: bold;
    }
</style>
<body>
    <form id="form1" runat="server">
        <h1>Escolha os dias da semana que você está disponível para trabalhar!</h1>
        <hr>
        <div class="formulario">
            <asp:CheckBoxList ID="clbDias" runat="server" Height="186px" Width="126px">
                <asp:ListItem Value="Domingo">Domingo</asp:ListItem>
                <asp:ListItem Value="Segunda">Segunda-Feira</asp:ListItem>
                <asp:ListItem Value="Terca">Terça-Feira</asp:ListItem>
                <asp:ListItem Value="Quarta">Quarta-Feira</asp:ListItem>
                <asp:ListItem Value="Quinta">Quinta-Feira</asp:ListItem>
                <asp:ListItem Value="Sexta">Sexta-Feira</asp:ListItem>
                <asp:ListItem Value="Sabado">Sábado</asp:ListItem>
            </asp:CheckBoxList>
        </div>
        <div class="btn">
            <asp:Button ID="btnEnviar" runat="server" Text="Enviar" OnClick="btnEnviar_Click" Height="30px" Width="60px" />
        </div>
        <div class="lbl">
            <asp:Label ID="lblResultado" runat="server" Text="Dia(s):"></asp:Label>
        </div>
    </form>
</body>
</html>
