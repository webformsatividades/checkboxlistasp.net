﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CheckBoxList
{
    public partial class Formulario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            var diasMarcados = clbDias.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Text);

            if (diasMarcados.Count() >= 1)
                lblResultado.Text = "Dia(s) selecionado(s): " + string.Join(", ", diasMarcados);
            else
                lblResultado.Text = "Nenhum dia selecionado!";
        }
    }
}